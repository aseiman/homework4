import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

   static double delta = Math.pow(10, -12);
   private double a, b, c, d;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return this.a;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return this.b;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return this.c;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return this.d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
	  return d2s(this.a) + d2s(this.b) + "i" + d2s(this.c) + "j" + d2s(this.d) + "k";
   }

   private String d2s(double x) {
	   String s = Double.toString(x);
	   if (s.charAt(0) != '+' && s.charAt(0) != '-') {
		   s = "+" + s;
	   }
	   return s;
   }   
   
   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) throws IllegalArgumentException {

	  String[] stringList = s.split("(?=\\+)|(?=\\-)");	  
	  int f = 0;
	  while (stringList[f] == null || stringList[f].equals("")) {
		  f++;
	  }
	  
	  if (stringList.length - f != 4) {		  
		  throw new IllegalArgumentException("Error in string: " + s);
	  }
	  	
	  if (!(stringList[f+1].charAt(stringList[f+1].length() - 1) == 'i')) throw new IllegalArgumentException("Error in string: " + s);
	  if (!(stringList[f+2].charAt(stringList[f+2].length() - 1) == 'j')) throw new IllegalArgumentException("Error in string: " + s);
	  if (!(stringList[f+3].charAt(stringList[f+3].length() - 1) == 'k')) throw new IllegalArgumentException("Error in string: " + s);
	  
	  double a0 = Double.parseDouble(stringList[f+0]);         
      double b0 = Double.parseDouble(stringList[f+1].substring(0, stringList[f+1].length() - 1));
      double c0 = Double.parseDouble(stringList[f+2].substring(0, stringList[f+2].length() - 1));
      double d0 = Double.parseDouble(stringList[f+3].substring(0, stringList[f+3].length() - 1));
      
	  try {
         double a = Double.parseDouble(stringList[f+0]);         
         double b = Double.parseDouble(stringList[f+1].substring(0, stringList[f+1].length() - 1));
         double c = Double.parseDouble(stringList[f+2].substring(0, stringList[f+2].length() - 1));
         double d = Double.parseDouble(stringList[f+3].substring(0, stringList[f+3].length() - 1));         
         return new Quaternion(a, b, c, d);         
	  } catch (Exception e) {
		  throw new IllegalArgumentException("Error in string: " + s);		  
	  }
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(this.a, this.b, this.c, this.d);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      
	  if (compDoubles(a, 0.) && compDoubles(b, 0.) && compDoubles(c, 0.) && compDoubles(d, 0.)) {
		  return true;
	  } else {
		  return false;
	  }
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
	  return new Quaternion(this.a, -1*this.b, -1*this.c, -1*this.d); 
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
	  return new Quaternion(-1*this.a, -1*this.b, -1*this.c, -1*this.d);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(this.a + q.getRpart(), this.b + q.getIpart(), this.c + q.getJpart(), this.d + q.getKpart());
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
	  double a1, a2, b1, b2, c1, c2, d1, d2;
	  a1 = this.a;
	  b1 = this.b;
	  c1 = this.c;
	  d1 = this.d;
	  
	  a2 = q.getRpart();
	  b2 = q.getIpart();
	  c2 = q.getJpart();
	  d2 = q.getKpart();
      
	  return new Quaternion(a1*a2-b1*b2-c1*c2-d1*d2, a1*b2+b1*a2+c1*d2-d1*c2, a1*c2-b1*d2+c1*a2+d1*b2, a1*d2+b1*c2-c1*b2+d1*a2);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
	  return new Quaternion(r*this.a, r*this.b, r*this.c, r*this.d);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
	  if (this.isZero()) {
		  throw new RuntimeException("Inverse of quaternion can not be performed as quaternion iz zero.");
	  }
	  double a, b, c, d;
	  a = this.a;
	  b = this.b;
	  c = this.c;
	  d = this.d;		  
	  return new Quaternion(a/(a*a+b*b+c*c+d*d) ,(-b)/(a*a+b*b+c*c+d*d), (-c)/(a*a+b*b+c*c+d*d), (-d)/(a*a+b*b+c*c+d*d));
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return this.plus(q.opposite());
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      return this.times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {	  
      return q.inverse().times(this);
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
       if (!(qo instanceof Quaternion)) { 
	      return false;		   
       }
       
	   double a1, a2, b1, b2, c1, c2, d1, d2;
	   	   
	   a1 = this.a;
	   b1 = this.b;
	   c1 = this.c;
	   d1 = this.d;
		
	   Quaternion q = (Quaternion)qo;
       a2 = q.getRpart();
	   b2 = q.getIpart();
	   c2 = q.getJpart();
	   d2 = q.getKpart();
	   
	   if (compDoubles(a1, a2) && compDoubles(b1, b2) && compDoubles(c1, c2) && compDoubles(d1, d2)) {
		   return true; 
	   } else {		   
		   return false;
	   }
   }

   private boolean compDoubles(double x, double y) {
      if (Math.abs(x - y) < delta) {
    	  return true;
      } else {
    	  return false;
      }      
   }
   
   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
	  Quaternion temp1 = this.times(q.conjugate());
	  Quaternion temp2 = q.times(this.conjugate());
	  Quaternion temp3 = temp1.plus(temp2); 
	  return temp3.times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      int hash = 0;
      hash = hash * 11 + Math.abs((int)(13 * this.a)) + (int)Math.signum(this.a);
      hash = hash * 17 + Math.abs((int)(19 * this.b)) + (int)Math.signum(this.b);
      hash = hash * 23 + Math.abs((int)(31 * this.c)) + (int)Math.signum(this.c);
      hash = hash * 37 + Math.abs((int)(41 * this.d)) + (int)Math.signum(this.d);
	  return hash;
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.pow(this.a*this.a + this.b*this.b + this.c*this.c + this.d*this.d, 0.5);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
	  // original code
	  Quaternion q = new Quaternion(1.0,1.0,1.0,1.0);
	  q.valueOf(".9+.2i+.3j+.4k");
	   
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
      
      System.out.println(valueOf("1-2i+4.5j-3k"));
   }
}
// end of file
